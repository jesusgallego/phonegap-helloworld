function onLoad() {
  document.addEventListener('deviceready', onDeviceReady, this);
}

function onDeviceReady() {
  document.addEventListener("backbutton", backKeyDown, true);
}

function backKeyDown() {
  navigator.notification.alert();
}

function back() {
  window.location = "index.html"
}
